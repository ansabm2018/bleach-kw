from django import forms
from user.models import UserProfile


class StudentRegisterForm(forms.ModelForm):
    email = forms.EmailField(max_length=200, help_text='Required')

    class Meta:
      model = UserProfile
      fields = ('name','gender','mobile_no','email_adderss','photo','date_of_birth','blood_group','religion','mother_tongue','country','state','city','place_of_residence','pin','permanent_address','current_address','date_of_join','fathers_name','fathers_qualification','fathers_occupation','fathers_mobile','fathers_email','mothers_name','mothers_qualification','mothers_occupation','mothers_mobile','mothers_email','guardians_name','guardians_relation','guardians_mobile1','guardians_mobile2') 

    def __init__(self, *args, **kwargs):
        super(StudentRegisterForm, self).__init__(*args, **kwargs)

        self.fields['name'].required = True