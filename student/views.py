from django.shortcuts import render,redirect
from django.views import View
from django.contrib import messages 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from bleachkw.utils import get_error
import string    
import random
from django.db.models import Q
import functools
import operator

from user.models import UserProfile
from student.forms import StudentRegisterForm


#for unique order id
def random_string_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_username_generator():
    new_username = random_string_generator()

    username_exists    = UserProfile.objects.filter(username=new_username).exists()
    if username_exists:
        return unique_order_id_generator()
    return new_username


# Create your views here.

class StudentHome(View):
	def get(self,request):
		page 		= 	self.request.GET.get('page',1)
		
		try:
			students = UserProfile.objects.filter(is_active=True)
		except:
			students = None
        
		fil_query           = request.GET.get('search')	
		fil_byname          = request.GET.get('filter_name')
		
		#search filter
		if fil_query:
			students = students.filter(Q(Q(name__icontains=fil_query)))
		
		#ascending descending filter
		if fil_byname:
			if  fil_byname == 'ascending':
				students = students.order_by('name')
			elif fil_byname == 'descending':
				students = students.order_by('-name')	
			else:
				pass

		fil_gender          = request.GET.get('fil_gender')
		fil_bloodgroup      = request.GET.get('fil_bloodgroup')	

		#Student table other filters 
		filters=[] 
		if fil_gender: 
		    case1 = Q(gender=fil_gender)
		    filters.append(case1)
		if fil_bloodgroup:
		    case2 = Q(blood_group=fil_bloodgroup)
		    filters.append(case2)          

		if fil_gender or fil_bloodgroup: 
			filters=functools.reduce(operator.and_,filters)
			students=students.filter(filters)	

		paginator	=	Paginator(students,100)
		
		try:
			students	=	paginator.page(page)		
		except PageNotAnInteger:
			students	=	paginator.page(1)
		except EmptyPage:
			students 	= 	paginator.page(paginator.num_pages)
		
		index 		= 	students.number - 1
		max_index 	= 	len(paginator.page_range)
		start_index = 	index - 6 if index >= 6 else 0
		end_index 	= 	index + 6 if index <= max_index - 6 else max_index
		page_range 	= 	list(paginator.page_range)[start_index:end_index]

		return render(request,'student/student_list.html',{"page_range":page_range,"students":students,"fil_query":fil_query,"fil_bloodgroup":fil_bloodgroup,"fil_gender":fil_gender,"fil_byname":fil_byname,})

class StudentAdd(View):
	def get(self,request):

		student_reg_form = StudentRegisterForm()
		
		return render(request,'student/student_add.html',{"student_reg_form":student_reg_form})	

	def post(self,request):
		student_reg_form = StudentRegisterForm(request.POST,request.FILES or None)
		
		if student_reg_form.is_valid():
			student_reg_form_save          = student_reg_form.save(commit=False)
			student_reg_form_save.username = unique_username_generator()
			student_reg_form_save.user_type= 'STUDENT'
			student_reg_form_save.save()
		else:			
			messages.error(request,get_error(student_reg_form))			
			return render(request,'student/student_add.html',{"student_reg_form":student_reg_form})
		return redirect('student_home')	

class StudentEdit(View):
	def get(self,request,student_id):

		try:
			student = UserProfile.objects.get(id=student_id)
		except:
			student = None	

		student_edit_form = StudentRegisterForm(instance=student)
		
		return render(request,'student/student_edit.html',{"student_edit_form":student_edit_form})	

	def post(self,request,student_id):

		try:
			student = UserProfile.objects.get(id=student_id)
		except:
			student = None	

		student_edit_form = StudentRegisterForm(request.POST,request.FILES or None,instance=student)
		

		if student_edit_form.is_valid():
			student_edit_form_save          = student_edit_form.save(commit=False)
			student_edit_form_save.save()
		else:			
			messages.error(request,get_error(student_edit_form))			
			return render(request,'student/student_edit.html',{"student_edit_form":student_edit_form})
		return redirect('student_home')

class StudentDelete(View):
	def get(self,request,student_id):
		try:
			student_delete = UserProfile.objects.filter(id=student_id).update(is_active=False)
		except:
			student_delete = None
		
		if student_delete:
			messages.success(request,"Student Succesfully Deleted")
		else:
			messages.error(request,"An error occured !!!")

		return redirect('student_home')	