from django.conf.urls import url, include
from django.conf import settings

from student import views
 


 
urlpatterns=[

		url(r'^student_add/$',views.StudentAdd.as_view(),name='student_add'),
		url(r'^student_edit/(?P<student_id>\d+)$',views.StudentEdit.as_view(),name='student_edit'),
		url(r'^student_delete/(?P<student_id>\d+)$',views.StudentDelete.as_view(),name='student_delete'),

   ]