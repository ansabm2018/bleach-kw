from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from user.models import UserProfile
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
# Register your models here.


class CustomUserCreationForm(UserCreationForm): 
    class Meta:
        model = UserProfile
        fields = ("username",)

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = UserProfile
        exclude=[]

class CustomUserAdmin(UserAdmin):
 
    fieldsets = (
        (None, {'fields': ('username', 'password','email')}),
        (_('Personal info'), {'fields': ('user_type','gender','mobile_no','email_adderss','photo','date_of_birth','blood_group','religion','mother_tongue','country','state','city','place_of_residence','pin','permanent_address','current_address','date_of_join','fathers_name','fathers_qualification','fathers_occupation','fathers_mobile','fathers_email','mothers_name','mothers_qualification','mothers_occupation','mothers_mobile','mothers_email','guardians_name','guardians_relation','guardians_mobile1','guardians_mobile2',)}),

        (_('Permissions'), {'fields': ('is_active', 'is_staff','is_superuser',
                                       'groups', 'user_permissions')}),
        (_('dates'), {'fields': ('last_login',  )}),
    )
    
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2')}
        ),
    )

    form      = CustomUserChangeForm 
    add_form  = CustomUserCreationForm


admin.site.register(UserProfile, CustomUserAdmin)
