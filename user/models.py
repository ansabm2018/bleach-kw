from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.utils import timezone

USER_TYPE_CHOICES=(   
    ('STUDENT','STUDENT'),         
)

BLOOD_GROUP_CHOICES=( 
	("A Positive","A Positive"), 
    ("A Negative","A Negative"),
    ("B Positive","B Positive"),
    ("B Negative","B Negative"),
    ("O Positive","O Positive"),
    ("O Negative","O Negative"),
    ("AB Positive","AB Positive"),
    ("AB Negative","AB Negative")
)

RELIGION_CHOICES=(
	('ISLAM','ISLAM'),
	('MUSLIM','MUSLIM'),
	('CHRISTIAN','CHRISTIAN'),
	('HINDU','HINDU'),
	('NO_RELIGION','No Religion'),
)

GENDER_CHOICES=(
	('MALE','MALE'),
	('FEMALE','FEMALE'),
	)

class CustomUserManager(BaseUserManager):
    def _create_user(self, username, password, 
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username and password.
        """
        now = timezone.now() 
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(username=username,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        return self._create_user(username, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        return self._create_user(username, password, True, True,
                                 **extra_fields)


class UserProfile(AbstractUser):
    # username
	# email
	# password
	# first_name
	user_type            =   models.CharField(max_length=100,choices=USER_TYPE_CHOICES,null=True,blank=True)
	name                 =	 models.CharField(max_length=100,blank=True,null=True)
	gender               =   models.CharField(max_length=10,null=False,default='MALE',choices=GENDER_CHOICES)
	mobile_no            =   models.CharField(max_length=30,verbose_name='Mobile number',null=True,blank=True)
	email_adderss        =   models.EmailField(null=True,blank=True)
	photo				 =	 models.ImageField(upload_to='profile/',null=True,blank=True)
	date_of_birth        =   models.DateField(null=True,blank=True)
	blood_group          =   models.CharField(max_length=25,null=True,blank=True,choices=BLOOD_GROUP_CHOICES)
	religion             =   models.CharField(max_length=25,null=True,blank=True,choices=RELIGION_CHOICES)
	mother_tongue        =   models.CharField(max_length=25,null=True,blank=True)

	country              =   models.CharField(max_length=100,null=True,blank=True)
	state                =   models.CharField(max_length=100,null=True,blank=True)
	city                 =   models.CharField(max_length=100,null=True,blank=True)
	place_of_residence	 =	 models.CharField(null=True,blank=True,max_length=255)
	pin					 =	 models.IntegerField(null=True,blank=True)
	permanent_address    =   models.TextField(null=True,blank=True)
	current_address      =   models.TextField(null=True,blank=True)
	date_of_join         =   models.DateField(null=True,blank=True)
	
	fathers_name         =   models.CharField(max_length=100,null=True,blank=True)
	fathers_qualification=   models.CharField(max_length=100,null=True,blank=True)
	fathers_occupation   =   models.CharField(max_length=100,null=True,blank=True)
	fathers_mobile       =   models.CharField(max_length=30,verbose_name='Father\'s Mobile number',null=True,blank=True)
	fathers_email        =   models.EmailField(null=True,blank=True)
	mothers_name         =   models.CharField(max_length=100,null=True,blank=True)
	mothers_qualification=   models.CharField(max_length=100,null=True,blank=True)
	mothers_occupation   =   models.CharField(max_length=100,null=True,blank=True)
	mothers_mobile       =   models.CharField(max_length=30,verbose_name='Mother\'s Mobile number',null=True,blank=True)
	mothers_email        =   models.EmailField(null=True,blank=True)
	guardians_name       =   models.CharField(max_length=200,null=True,blank=True,verbose_name="Guardian's name")
	guardians_relation   =   models.CharField(max_length=200,null=True,blank=True,verbose_name="Guardian's relation with student")
	guardians_mobile1    =   models.CharField(max_length=30,verbose_name='Guardian\'s Mobile number 1',null=True,blank=True)
	guardians_mobile2    =   models.CharField(max_length=30,verbose_name='Guardian\'s Mobile number 2',null=True,blank=True)
	

	objects=CustomUserManager()

	def __unicode__(self):
		return str(self.username) 

	def __str__(self):
		return str(self.username)
