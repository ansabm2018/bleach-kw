// $("#id_category").change(function () {
//     product_category = this.value;
//     $.ajax({
//         url:"/customer/ajax/productcategory/filter/",
//         data: {'value':product_category}, 
//         dataType: 'json',
//         success: function (data) { 
//           var ret = data;   
//         $('#id_model').empty();
//         $('#id_sub_model').empty();
//         $('#id_year').empty();

//         $("#id_model").append($('<option>', {
//           value: "", 
//          text: "Model" 
//       }));    

//         $("#id_sub_model").append($('<option>', {
//           value: "", 
//          text: "Submodel"
//       }));

//         $("#id_year").append($('<option>', {
//           value: "", 
//          text: "Year"
//       }));

//         $.each( ret.pmodels, function( key, values ) {
//               $("#id_model").append($('<option>', {
//     value: key, 
//     text: values
// }));
//               });  

//         } 
//       }); 
//   });

// $("#id_model").change(function () {
//     product_model = this.value;
//     $.ajax({
//         url:"/customer/ajax/productcategory/filter/",
//         data: {'value2':product_model}, 
//         dataType: 'json',
//         success: function (data) { 
//           console.log('drm')
//           var ret = data;  
//         $('#id_sub_model').empty();
//         $('#id_year').empty();
            
//         $("#id_sub_model").append($('<option>', {
//           value: "", 
//          text: "Submodel"
//       }));
//         $("#id_year").append($('<option>', {
//           value: "", 
//          text: "Year"
//       }));

//         $.each( ret.submodels, function( key, values ) {
//               $("#id_sub_model").append($('<option>', {
//     value: key, 
//     text: values  
// }));
// });  
  

//         } 
//       }); 
  
//   });

// $("#id_sub_model").change(function () {
//     product_submodel = this.value;
//     $.ajax({
//         url:"/customer/ajax/productcategory/filter/",
//         data: {'value3':product_submodel}, 
//         dataType: 'json',
//         success: function (data) { 
//           console.log('drm')
//           var ret = data;  
//         $('#id_year').empty();
        
//         $("#id_year").append($('<option>', {
//           value: "", 
//          text: "Year"
//       }));

//         $.each( ret.pyears, function( key, values ) {
//               $("#id_year").append($('<option>', {
//     value: key, 
//     text: values  
// }));
// });  

//         } 
//       }); 
  
//   });


//new things default
$(".category_class").click(function () {

    product_category = $(this).data("id");
    $('#productcategory_id').val(product_category);

    $('.make-head').removeClass("active");
    $('.make-head').html($(this).data("name"));
    $('#category_id').removeClass("active");
    $(".model-head").addClass("active");
    
    $(".model-head").removeClass('disabled');
    $.ajax({
        url:"/customer/ajax/productcategory/filter/",
        data: {'value':product_category}, 
        dataType: 'json',
        success: function (data) { 
          var ret = data;   
        $('#model_id').empty();
        $('#sub_model_id').empty();
        $('#year_id').empty();

        $.each( ret.pmodels, function( key, values ) {
              $("#model_id").append('<button class="model_class" data-id="'+key+'" data-name="'+values+'">'+values+'</button>');             
              });
              } 
      }); 
    $("#model_id").addClass("active");
  });


$(document).on('click', '.model_class', function(){
    product_model = $(this).data("id");
    $('#productmodel_id').val(product_model);
    
    $(".model-head").removeClass("active");
    $('.model-head').html($(this).data("name"));
    $("#model_id").removeClass("active");
    $(".submodel-head").addClass("active");
    
    $(".submodel-head").removeClass('disabled');
    $.ajax({
        url:"/customer/ajax/productcategory/filter/",
        data: {'value2':product_model}, 
        dataType: 'json',
        success: function (data) { 
          var ret = data;  
        $('#sub_model_id').empty();
        $('#year_id').empty();            

        $.each( ret.submodels, function( key, values ) {
              $("#sub_model_id").append('<button class="sub_model_class" data-id="'+key+'" data-name="'+values+'">'+values+'</button>');
              });
              } 
      });
      $("#sub_model_id").addClass("active"); 
  });

 
$(document).on('click', '.sub_model_class', function(){
    product_submodel = $(this).data("id");
    $('#productsubmodel_id').val(product_submodel);
    
    $(".submodel-head").removeClass("active");
    $('.submodel-head').html($(this).data("name"));
    $("#sub_model_id").removeClass("active");
    $(".year-head").addClass("active");
    
    $(".year-head").removeClass("disabled");
    $.ajax({
        url:"/customer/ajax/productcategory/filter/",
        data: {'value3':product_submodel}, 
        dataType: 'json',
        success: function (data) { 
          var ret = data;  
        $('#year_id').empty();            

        $.each( ret.pyears, function( key, values ) {
              $("#year_id").append('<button class="year_class" data-id="'+key+'" data-name="'+values+'">'+values+'</button>');
              });
              } 
      }); 
    $("#year_id").addClass("active");
  }); 

$(document).on('click', '.year_class', function(){
    product_year = $(this).data("id");
    $('#productyear_id').val(product_year);
    
    $(".year-head").removeClass("active");
    $('.year-head').html($(this).data("name"));
    $("#year_id").removeClass("active");
  });  

//new things dynamic
$('.make-head').click(function(){
  $('#category_id').addClass("active");
  $("#model_id").removeClass("active");
  $("#sub_model_id").removeClass("active");
  $("#year_id").removeClass("active");
  $(".model-head").removeClass("active");
  $(".submodel-head").removeClass("active");
  $(".year-head").removeClass("active");
  $(".category-head").addClass("active");

  $('.model-head').html("Model");
  $('.submodel-head').html("Submodel");
  $('.year-head').html("Year");

  });
$('.model-head').click(function(){
  $("#model_id").addClass("active");
  $("#sub_model_id").removeClass("active");
  $("#year_id").removeClass("active");
  $('#category_id').removeClass("active");
  $(".submodel-head").removeClass("active");
  $(".year-head").removeClass("active");
  $(".category-head").removeClass("active");
  $(".model-head").addClass("active");

  $('.submodel-head').html("Submodel");
  $('.year-head').html("Year");
  
  });
$('.submodel-head').click(function(){
  $("#sub_model_id").addClass("active");
  $("#year_id").removeClass("active");
  $('#category_id').removeClass("active");
  $("#model_id").removeClass("active");
  $(".year-head").removeClass("active");
  $(".category-head").removeClass("active");
  $(".model-head").removeClass("active");
  $(".submodel-head").addClass("active");

  $('.year-head').html("Year");
  });
$('.year-head').click(function(){
  $("#year_id").addClass("active");
  $('#category_id').removeClass("active");
  $("#model_id").removeClass("active");
  $("#sub_model_id").removeClass("active");
  $(".category-head").removeClass("active");
  $(".model-head").removeClass("active");
  $(".submodel-head").removeClass("active");
  $(".year-head").addClass("active");
  });