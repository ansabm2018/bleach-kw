$(window).ready(function() {
  // pre-loader script 
  $(".pre-loader").fadeOut("slow");
  
  // active link 
  for (var i = 0; i < document.links.length; i++) {
   if (document.links[i].href == document.URL) {
   $(document.links[i]).addClass('active');
 }}
  
});

// close-modal
$('.close-modal, .pop-up .modal-footer .delete-btn').on('click',function() {
  $('.modal').modal('hide');
});

 // toggle-password
 $(".toggle-password").click(function() {
  //$(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
  });
  
// submitAnyForm
function submitAnyForm(src) 
    {
    var form=$(src).parents('form').first();
    if(! form[0].checkValidity())
        $('<input type="submit">').hide().appendTo(form).click().remove();
    else
        {
        $(src).removeAttr('onclick');
        $(src).prop('onclick',null).off('click');
        $(src).text('Submitting...');
        form.submit();
        }
  }

// profile_popup
$('.profile_popup_btn').click(function() {
	$('.profile_popup').toggleClass('profile_popup_open');
});

// date_pick   
$(function () {
  $('.date_pick').datetimepicker({ 
    pickTime: false, 
    format: "DD-MM-YYYY", 
    maxDate: 'now().date()', 
  });
});

// checked_input
var $radioButtons = $('.radio-cbox-btn label input');
$radioButtons.click(function() {
    $radioButtons.each(function() {
        $(this).parent().toggleClass('checked_input', this.checked);
    });
});

// Confirm password front-end validation script  
function chkPasswordMatch() {
  var password = $("#comm_confirm_password").val();
  var confirmPassword = $("#comm_new_password").val();
  if (password != confirmPassword){
      $("#comm_password_match").html("Passwords do not match!");
      $("#comm_change_password_btn").attr('disabled',true);
  }
  else{
      $("#comm_password_match").html("Passwords match.");
      $("#comm_change_password_btn").attr('disabled',false);
  }
}
$(document).ready(function () {
    $("#comm_confirm_password, #comm_new_password").keyup(chkPasswordMatch);
});

// jquery-file-upload-preview-remove-image
// https://stackoverflow.com/questions/40609463/jquery-file-upload-preview-remove-image
function readURL() {
  var $input = $(this);
  var $newinput =  $(this).parent().find('.choose_img_show');
  if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $newinput.attr('src', e.target.result).show();
      }
      reader.readAsDataURL(this.files[0]);
  }
}

$(".choose_img").change(readURL);

//Image Validation
$(".img_validate").change(function() {
        var val = $(this).val();
        $('.image_validation_msg').remove(); 
        if(this.files[0].size>1100000)
        {
          $(this).after('<span class="image_validation_msg">Upto 1MB File Upload is Possible.</span>');
        }

        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'gif': case 'jpg': case 'png': case 'jpeg': case 'bmp':
                break;
            default:
                $(this).val('');
                // error message here
                $(this).after('<span class="image_validation_msg">Only JPG,PNG,BMP and JPEG supported.</span>');
                break;
        }
});