$(window).ready(function() {
   $('ul.sub-menu li a.active').first().parents('ul').first().addClass('open-sub-menu');
   
   // page-form, pop-up 'col height-set'
   $('.page-col textarea').parents('.page-col').addClass('height-set');
   $('.pop-up-col textarea').parents('.pop-up-col').addClass('height-set');
   
  // page-form, pop-up 'choose-img-div'
   $('.page-col .choose-img-show').parents('.page-col').addClass('choose-img-div');
   $('.pop-up-col .choose-img-show').parents('.pop-up-col').addClass('choose-img-div');
   
});
 

// add-field and remove
$('.multi_field_wrapper').each(function() {
  var $wrapper = $('.multi-fields', this); 
  $(".add-field-btn", $(this)).click(function(e) {
    var filed = $('.multi-field:first-child', $wrapper).clone(true);
      filed.appendTo($wrapper).find('textarea').val('')
      filed.appendTo($wrapper).find('input').val('').focus()
      filed.appendTo($wrapper).find('img').val('').focus().prop('src', "/static/files/images/default-img.png");
});
  $('.multi-field .remove-field-btn', $wrapper).click(function() {
      if ($('.multi-field', $wrapper).length > 1)
          $(this).parent('.multi-field').remove();
  });
});


// vertical_drop_menu
$('.vertical_drop_menu .drop_menu_btn ul').hide();
$(".vertical_drop_menu .drop_menu_btn .drop_a_btn").click(function () {
	$(this).parent(".vertical_drop_menu .drop_menu_btn").children("ul").slideToggle("100");
	$(this).find(".drop_icon").toggleClass("fa-plus fa-minus");
});
$(".vertical_drop_toggle_btn").click(function () {
	$('body').toggleClass("vertical_drop_menu_open");
});

// hid-show-menu
/*
var div = localStorage['div'] || 0;
  if (div == 1) {
  $('body').toggleClass("hid-show-menu-click");
  }
  $('.hid-show-menu').click(function() {
	$('body').toggleClass("hid-show-menu-click");
	var current = localStorage['div'] || 0;
	if (current == 1) {
	  localStorage['div'] = 0;
	 } 
	else {
	localStorage['div'] = 1;
	}
}); 
*/

// open_settings
$(".settings_btn").on("click", function(){
  $(this).parents(".simple-card-col").toggleClass("open_settings");
});

// front-end table search
!function(t){"use strict";var e=function(e){function n(n){o=n.target;var r=t.getElementsByClassName(o.getAttribute("data-table"));e.forEach.call(r,function(t){e.forEach.call(t.tBodies,function(t){e.forEach.call(t.rows,a)})})}function a(t){var e=t.textContent.toLowerCase(),n=o.value.toLowerCase();t.style.display=-1===e.indexOf(n)?"none":"table-row"}var o;return{init:function(){var a=t.getElementsByClassName("filter");e.forEach.call(a,function(t){t.oninput=n})}}}(Array.prototype);t.addEventListener("readystatechange",function(){"complete"===t.readyState&&e.init()})}(document);

